# Ebay-selenide


## General info
The main goal of project - show the work code of UI tests for eBay.

## Technologies
* JDK 1.8;
* Maven;
* Selenide;
* Page Object;
* TestNG;
* Allure;

## Setup
To run this project:
1. To run the test, go to project directory and run the commands:
```
$ mvn clean test
```
2. To look the results of tests, run the command:
```
$ mvn site
```
2.1 Go to directory with results report (/target/site/allure-maven.html) and open it in browser.
![Allure report](https://i.ibb.co/HqhZSZQ/Allure.png)