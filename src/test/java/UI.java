import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.*;
import ru.yandex.qatools.allure.annotations.Description;
import setup.setupChrome;

import java.util.ResourceBundle;

import static com.codeborne.selenide.Selenide.close;
import static com.codeborne.selenide.Selenide.open;

public class UI {

    private Main main = new Main();
    private SignIn signIn = new SignIn();
    private Search search = new Search();
    private DescriptionGoods descriptionGoods = new DescriptionGoods();
    private WatchList watchList = new WatchList();
    private Shopping shopping = new Shopping();
    private Profile profile = new Profile();
    private ResourceBundle credentials = ResourceBundle.getBundle("credentials");
    private ResourceBundle testData = ResourceBundle.getBundle("testData");
    private ResourceBundle pages = ResourceBundle.getBundle("pages");



    @BeforeMethod
    public static void start(){ setupChrome.openChrome(); }

    @Test(priority = 0)
    @Description("Check the positive login")
    public void login(){
        main.gotoSingInPage();
        signIn.SignIn((credentials.getString("username")),(credentials.getString("password")));
        main.assertMainPage();
    }

    @Test(dependsOnMethods = {"login"})
    @Description("Test the searching of goods by his full name")
    public void test01(){
        login();
        main.searchGoods((testData.getString("canon")));
        search.searchCanon();
    }

    @Test(dependsOnMethods = {"login"})
    @Description("Testing of filters for searching of goods")
    public void test02(){
        login();
        main.searchGoods("Samsung");
        search.filterforSamsung();
    }

    @Test(dependsOnMethods = {"login"})
    @Description("Testing of function WatchList of goods")
    public void test03(){
        login();
        open((pages.getString("SamsungGalaxyS6")));
        descriptionGoods.addToWatchList();
        watchList.watchList();
    }

    @Test(dependsOnMethods = {"login"})
    @Description("Testing of total price for two goods")
    public void test04() throws InterruptedException{
        login();
        shopping.removeOldGoods();
        open((pages.getString("SamsungGalaxyS6")));
        descriptionGoods.addToCard();
        shopping.checkSamsungInShopping();
        open((pages.getString("iPhone7")));
        descriptionGoods.addToCard();
        shopping.checkPriceTwoGoods();
    }

    @Test(dependsOnMethods = {"login"})
    @Description("Testing of function for changing of avatars")
    public void test05(){
        login();
        main.gotoAvatarPage();
        profile.uploadNewAvatar();
    }

    @AfterMethod
    public void closeBrowser(){
        close();
    }


}
