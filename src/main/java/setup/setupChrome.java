package setup;

import com.codeborne.selenide.Configuration;

import static com.codeborne.selenide.Selenide.open;

public class setupChrome {

    public static void openChrome(){
        Configuration.browser="chrome";
        Configuration.timeout = 4000;
        open("https://www.ebay.com/");
    }
}
