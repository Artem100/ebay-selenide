package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import static com.codeborne.selenide.Selenide.$;

public class DescriptionGoods {

    private By buttonAddtoWatchList = By.cssSelector("a#watchLink");
    private By messageAddList = By.id("w1-6-_lmsg");
    private By iconWatchList = By.cssSelector("div.msgPad");
    private By gotoWatchList = By.cssSelector("#w1-6-_lmsg > a");
    private By selectColor = By.name("Color");
    private By buttonAddToCard = By.cssSelector("a#isCartBtn_btn");
    private By buttonBuyNow = By.cssSelector("a.btn--primary[data-action-name='BUY_IT_NOW']");

    public void addToWatchList(){
        if ($(selectColor).isDisplayed()){
            Select dropdown= new Select($(selectColor));
            dropdown.selectByIndex(1);
            if($(buttonAddtoWatchList).isDisplayed()){$(buttonAddtoWatchList).click();}}
        else;
        $(iconWatchList).isDisplayed();
        $(messageAddList).getText().contains("Added to your Watch list");
        $(gotoWatchList).click();
    }

    public void addToCard(){
        selectColor();
        $(buttonAddToCard).click();
    }

    public void selectColor(){
        if ($(selectColor).isDisplayed()){
            Select dropdown= new Select($(selectColor));
            dropdown.selectByIndex(3);}
        else;
    }

}
