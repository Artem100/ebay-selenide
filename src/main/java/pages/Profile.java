package pages;

import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.ResourceBundle;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.title;

public class Profile {

    private ResourceBundle title = ResourceBundle.getBundle("titles");

    private By buttonEdit = By.id("edit");
    private By buttonUpdloadAvater = By.cssSelector("a.icons > form > input[name='imgbin']");
    private By buttonDoneEditing = By.cssSelector("button#done");
    private By buttonRemovePhoto = By.cssSelector("a.rmv");
    private By buttonChangePhoto = By.cssSelector("div.icon > div.acts > a.upld");
    private By buttonEditPhoto = By.cssSelector("a.icons.edit_icn.noUpld.pen");

    public void assertProfile(){
        Assert.assertTrue(title().contains(title.getString("profileOfMa_9843")));
    }

    public void uploadNewAvatar(){
        $(buttonEdit).click();
        if ($(buttonEditPhoto).isDisplayed()){
            $(buttonEditPhoto).click();
            $(buttonChangePhoto).click();
            $(buttonChangePhoto).sendKeys(System.getProperty("user.dir") + "/testFiles/dog.jpeg");
        } else {
            $(buttonUpdloadAvater).click();
            $(buttonUpdloadAvater).sendKeys(System.getProperty("user.dir") + "/testFiles/cat.jpg");
        }
        $(buttonDoneEditing).click();
    }



}
