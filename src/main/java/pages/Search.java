package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.testng.Assert;

import java.util.ResourceBundle;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class Search {

    private By checkboxSamsungGalaxyS6 = By.cssSelector("input[type='checkbox'][aria-label='Samsung Galaxy S6']");
    private By checkbox64GB = By.cssSelector("input[type='checkbox'][aria-label='64 GB']");
    private By checkbox3G = By.cssSelector("input[type='checkbox'][aria-label='3G']");
    private By checkboxConditionsNew = By.cssSelector("input[type='checkbox'][aria-label='Manufacturer refurbished']");
    private By fieldMaximumValue = By.cssSelector("input[aria-label='Maximum Value']");
    private By fieldMaximumValue2 = By.cssSelector("input.price[name='_udhi']");
    private By filter64GBDisplayed = By.xpath("//div[contains(text(),'64 GB')]/span[contains(@class, 'clipped')]");
    private By filterNewDisplayed = By.xpath("//div[contains(text(),'New')]/span[contains(@class, 'clipped')]");
    private By filter3GDisplayed = By.xpath("//div[contains(text(),'3G')]/span[contains(@class, 'clipped')]");
    private By filterSamsungS6Displayed = By.xpath("//div[contains(text(),'Samsung Galaxy S6')]/span[contains(@class, 'clipped')]");
    private By samsungS6Result1 = By.cssSelector("li.s-item");
    private By samsungS6Result2 = By.cssSelector("li.sresult"); // List
    private By clickSamsung1 = By.cssSelector("div.s-item__image-wrapper > .s-item__image-img");
    private By clickSamsung2 = By.cssSelector(".lvpicinner > .img.imgWr2");

    private ResourceBundle rb = ResourceBundle.getBundle("testData");


    public void searchCanon(){
        String wordForSearch = rb.getString("canonSearch");
        String searchResult = "//li[contains(@id, 'srp-river-results-listing1')]//h3[contains(text(),'"+wordForSearch+"')]";
        String searchResult2 = "li[r='1'] > h3.lvtitle > a";
        System.out.println(searchResult);
        System.out.println(searchResult2);
        Assert.assertTrue(($(By.xpath(searchResult))).isDisplayed()
                ||$((searchResult2)).getText().contains(wordForSearch));
    }

    public void fieldMaxiumPrice(String price){
        if($(fieldMaximumValue).isDisplayed()){
            $(fieldMaximumValue).sendKeys(price);
            $(fieldMaximumValue).sendKeys(Keys.ENTER);}
        else {
            $(fieldMaximumValue2).sendKeys(price);
            $(fieldMaximumValue2).sendKeys(Keys.ENTER);}
    }


    public void filterforSamsung(){
        fieldMaxiumPrice("230");
        $(checkboxSamsungGalaxyS6).click();
        $(checkbox64GB).click();
        $(checkbox3G).click();
        $(checkboxConditionsNew).click();
        Assert.assertTrue($$(samsungS6Result1).size()==1||$$(samsungS6Result2).size()==1);
    }

    public void clickSamsungS6(){
        if($(clickSamsung1).isDisplayed()) {
            $(clickSamsung1).click(); }
        else{ $(clickSamsung2).click(); }
    }

}
