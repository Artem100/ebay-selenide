package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.ResourceBundle;

import static com.codeborne.selenide.Selenide.*;

public class Shopping {

    private ResourceBundle title = ResourceBundle.getBundle("titles");
    private ResourceBundle pages = ResourceBundle.getBundle("pages");

    private By selectorGoods = By.cssSelector("div.cart-bucket-lineitem");
    private By fieldTotalCount = By.cssSelector("td.val-col.total-row > span > span");
    private By locatorSamsungGalaxyS6ActiveSMG890A = By.cssSelector("[data-test-info*='253059398565']");
    private By locatoriPhone7 = By.cssSelector("[data-test-info*='192677144331']");
    private By buttonRemove = By.cssSelector("[data-test-id='cart-remove-item']");

    public void assertShopping(){
        Assert.assertTrue(title().contains(title.getString("shopping"))); }

    public void checkSamsungInShopping(){
        assertShopping();
        Assert.assertTrue($$(selectorGoods).size()==1);
        Assert.assertTrue($(locatorSamsungGalaxyS6ActiveSMG890A).isDisplayed());
    }

    public void checkPriceTwoGoods()throws InterruptedException{
        assertShopping();
        Assert.assertTrue($$(selectorGoods).size()==2);
        Assert.assertTrue($(locatorSamsungGalaxyS6ActiveSMG890A).isDisplayed());
        Assert.assertTrue($(locatoriPhone7).isDisplayed());
        clickRemovebutton();
        Assert.assertTrue($$(selectorGoods).size()==0);
    }

    public void clickRemovebutton() throws InterruptedException{
        for (WebElement check : $$(buttonRemove)){
          check.click();}
        Thread.sleep(4000);
        }

    public void removeOldGoods() throws InterruptedException{
        open(pages.getString("CartPage"));
        assertShopping();
        clickRemovebutton();
    }

}
