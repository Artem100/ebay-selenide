package pages;

import com.codeborne.selenide.Selenide;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import java.util.ResourceBundle;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.title;

public class Main {
    private ResourceBundle titles = ResourceBundle.getBundle("titles");

    private By buttonSignIn = By.cssSelector(".gh-ug-guest > a");
    private By fieldSearch = By.id("gh-ac");
    private By buttonSearch = By.id("gh-btn");
    private By buttonProfile = By.cssSelector("button#gh-ug.gh-ua.gh-control");
    private By buttonAvatar = By.cssSelector("li#gh-up");


    public void assertMainPage(){
        Assert.assertTrue(title().contains(titles.getString("MainPage")));
    }

    public void gotoSingInPage(){
        assertMainPage();
        $(buttonSignIn).click();
    }

    public void searchGoods(String word){
        assertMainPage();
        $(fieldSearch).sendKeys(word);
        $(buttonSearch).click();
    }

    public void gotoAvatarPage(){
        assertMainPage();
        $(buttonProfile).click();
        $(buttonAvatar).click();
    }




}
