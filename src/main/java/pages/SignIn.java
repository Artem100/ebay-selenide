package pages;

import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.ResourceBundle;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.title;

public class SignIn {
    private ResourceBundle titles = ResourceBundle.getBundle("titles");

    private By fieldUsername = By.id("userid");
    private By fieldPassword = By.id("pass");
    private By buttonSubmit = By.id("sgnBt");

    public void assertSingInPage(){
        Assert.assertTrue(title().contains(titles.getString("SignIn")));
    }

    public void SignIn(String username, String password){
        assertSingInPage();
        $(fieldUsername).sendKeys(username);
        $(fieldPassword).sendKeys(password);
        $(buttonSubmit).click();
    }


}
