package pages;

import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.ResourceBundle;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.title;

public class WatchList {

    private ResourceBundle title = ResourceBundle.getBundle("titles");
    private ResourceBundle testData = ResourceBundle.getBundle("testData");

    private By titleOfGoods1 = By.cssSelector("div.item-title.hasvariations>a");
    private By checkBoxOfGoods = By.cssSelector(".action-clmn.item-clmn >.m-checkbox");
    private By buttonDelete = By.cssSelector("button.default.small.secondary");


    public void assertMainPage(){
        Assert.assertTrue(title().contains(title.getString("watchList")));
    }

    public void watchList(){
        assertMainPage();
        Assert.assertTrue($(titleOfGoods1).getText().contains(testData.getString("watchListSamsung")));
        $(checkBoxOfGoods).click();
        $(buttonDelete).click();
    }

}
